Carrier Python
===========================

How do I set it up?
-------------------
On Windows, you probably want to install Pycharm and Python from their respective
websites. Once downloaded, press the windows key and search for "Environment Variables".
The search result will either bring you to the Environment variable page or require you
to click a button at the bottom of the window. Once in the window titled
"Environment Variables", click on "Path" near the top of the screen and choose edit text.
Add (not remove what was in there) the following to the end of the text box:

%USERPROFILE%\\AppData\\Local\\Programs\\Python\\Python37-32;%USERPROFILE%\\AppData\\Local\\Programs\\Python\\Python37-32\\Scripts;%USERPROFILE%\\AppData\\Roaming\\Python\\Python37\\Scripts;

Now search for cmd or powershell and launch one of those programs. Type in:

pip install --user pipenv
and press enter.

We now have Python set up. In addition to Python, we need Git to be installed.
You can get that at: https://git-scm.com/download/win

The default settings are fine although you might want to change the editor from Vim
to something like Notepad.

With Git installed, go ahead and launch PyCharm and add a project from source control.
Select Git then paste in the URL: https://gitlab.com/ColoradoSchoolOfMines/carrier-python

Press test and you should get a success message. Once you import the project, open the
server.py file. PyCharm might ask you if you want to use PipEnv (you do) and if you want
to import the various dependencies (you want to do this too). That last one takes a
while.

If it doesn't ask those things, you can click the file menu, settings, Project:
<name>, gear icon, add, pipenv, ok.

Once it finishes, click the button to the left of the greyed out run button. In the window
that pops up, click the plus in the top left and select Python. There are a bunch of options
but we can ignore most of them. Just change the name to Server and use the folder icon
to find the server.py file. Press run and you should see the process start and do very little.
Click the drop down button next to the run button again and select edit configuration.
Do the same thing as before but select the client.py file. Run that one and a UI should
appear. Type localhost into the box and give yourself a name. If everything works,
you should be able to send messages to the server.

What is it?
-----------

The Python messenger uses a client server architecture to send data and messages
between clients. Unlike most other messengers, this one is designed with a
single goal held above all others: Be easy to extend. It accomplishes this goal
in the following ways:

1. Render messages as HTML. Instead of sending plain text, messages are expected
   to be renderable HTML documents. Of course HTML can easily support sending
   normal text, but it also supports easy rendering of different text styles,
   emojis, pictures, videos, interactive content, and more!

2. Open message structures. Messages are sent as json. Other than the action
   key and message key, no other keys are enforced. This means you can embed
   extra data in messages or send messages which normal clients won't render but
   a bot or plugin can use.

3. Support extensions through bots and plugins. Bots and
   plugins are designed to be fairly simple to create allowing people all sorts
   of power over the messenger. Bots can be created by
   copying the template.py file in the bots folder and modifying the parts
   marked TODO. See the echo bot for an example. Plugins are the same way except
   you want to use the plugins folder. The example plugin filters out any
   messages from the echo bot.

How do I use it?
----------------
First, install all of the needed requirements. If you aren't using PyCharm,
install Pyenv then run "pyenv install; pyenv sync -d" inside of the project
root. This will gather all of the needed dependencies. Next, run "pipenv shell"
to enter the project. Any editing or running should be done in the pipenv shell.

To run the server, find the server.py file and run it like "python server.py".
This will start the server on your machine. To run the client, run "python
client.py" and go through the prompts. To run a bot, use "python
bots/botname.py". Make sure to update the bot's code with the URL you want it to
connect to. Any file in the plugin folder is automatically loaded when the
client starts.

How do I make a bot?
--------------------
Everything you need should be inside the template.py file. Just copy that
and find the TODO's. You can debug your bot against a locally running server.

How do I make a plugin?
-----------------------
This process is nearly identical to making a bot except everything is in the
plugin folder. Plugins are automatically loaded by your client. If the plugin
crashes, it will likely crash the client as well.

How do I add a library?
-----------------------
This project uses Pipenv to manage dependencies. You can open the Pipfile
and add the library you want on under the packages section. Then, you run "pipenv
install" to install the new dependency.

What do messages look like?
---------------------------
A message comes in as a Json dictionary. If you're making a bot, the dictionary
has already been parsed into a normal dictionary for the on_message function.
There are few guarantees about the structure of messages you receive. First, the
message will have a key "type" which represents the type of the message. The
type in use right now is the "text" type. If a message is of type text, it must
also have a "content" key which stores the contents of the message. It is
assumed that the contents will be wrapped in a root div by the client before
being rendered. This wrapping will not touch the actual contents.

The above structure is not enforced by the server in any way. As a result, you
might want to prefer more structure checks over fewer checks as you might
accidentally crash your bot or the clients.

When sending a message, you need to wrap your message in a dictionary which
contains a key "action". Currently, the only supported action is "send". It
expects another key called "message" which stores the dictionary you want other
clients to receive. The file "bots/Template.py" has an example. Unlike when
receiving a message, the server will enforce that you send it something with
an "action" key or the "message" key. If you fail to do that, it will politely
kick you.

You can attach extra data which will be ignored when rendering the message.

How do I add my changes?
------------------------
This project uses a pretty standard process for accepting changes. First,
you need to fork the repository. https://docs.gitlab.com/ee/workflow/forking_workflow.html
has some information on this workflow. Once you've forked the process, you need to add the fork as
a new remote.

Using Pycharm, you should follow https://www.jetbrains.com/help/pycharm/set-up-a-git-repository.html#add-remote
where you want to find the instructions for adding a second remote.

If you want to use a CLI, the easiest way is to open up git bash on your computer and navigate to the project.
You can use cd to change directories and ls to see which files are available.
Most likely, you will need to run a command like "cd PycharmProjects/carrier-python/"
. Once you're in the project's folder, run "git remote add myFork <url to your project>".

Whichever option you pick will add a new remote named myFork which you can push to.
If you're unfamiliar with the CLI, Pycharm will let you commit then push to your fork by
following https://www.jetbrains.com/help/pycharm/commit-and-push-changes.html .

There's a chance that the main repo has been updated since you last pulled it. If you run into that problem,
just pull from origin then push back to myFork.

Once you're finished pushing, you can open up a pull request as described in the forking workflow link.

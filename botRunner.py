"""
Runs the bot passed as the first argument
"""
import asyncio
import json
import importlib
import sys
import websockets

class Main():
    def __init__(self, name):
        self.name = name
        self.websocket = None

        # Import the file manually (as apposed to the normal imports up above)
        module = importlib.import_module(f"bots.{name}")
        # Find the Plugin class in the file and create an instance of it
        self.bot = getattr(module, "Bot")()

    async def connect(self, location):
        """
        Called to start and run the bot
        """
        uri = f"ws://{location}:8765"
        websocket = await websockets.connect(uri)
        await websocket.recv()
        await websocket.send(self.name)
        response = await websocket.recv()
        if response == "?":
            print("Someone with the same name is already connected")
            exit()
        # Throw away previous messages
        response = await websocket.recv()
        self.websocket = websocket
        self.bot.websocket = self.websocket
        await self.socket_loop()


    async def socket_loop(self):
        """
        The main loop for the websocket part of the program.
        Called by connect.
        """
        while True:
            raw_message = await self.websocket.recv()
            message = json.loads(raw_message)
            # Make sure we didn't send the message
            if message["from"] != self.name:
                await self.bot.on_message(message)


# Run the program
if __name__ == "__main__":
    main = Main(sys.argv[1])
    asyncio.get_event_loop().run_until_complete(main.connect(sys.argv[2] if len(sys.argv) > 2 else "localhost"))
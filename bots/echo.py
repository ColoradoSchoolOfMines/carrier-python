"""
This is the client code for the python messenger.
The client is written using QT and websockets.
"""
import asyncio
import json
import websockets

class Bot():
    async def on_message(self, message):
        await self.websocket.send(json.dumps(message))
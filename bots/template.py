"""
This is the client code for the python messenger.
The client is written using QT and websockets.
"""
import asyncio
import json
import websockets

class Bot():
    async def on_message(self, message):
        """
        TODO Begin here if writing a bot.
        Whenever the bot receives a message, this function is called.
        Messages that you send will not trigger this function so don't
        worry about getting stuck in a loop. You can do anything you
        want in here. If you want to send a message, use
        "await self.websocket.send(json.dumps(message))" where message
        is the dictionary you want to send.

        Messages should be in the form of a dictionary.

        That dictionary should always contain a key "type" holding some string.
        If you want normal clients to see the message, "type" should point to
        "text" and another key, "content", should point to the contents of the
        message.

        For example, to send the word hello, message should be
        {"type": "text", "content": "hello"}
        """
class Plugin():
    channel = "home"

    def on_send(self, message):
        message["channel"] = self.channel

        if "content" in message and message["content"].startswith("/change "):
            self.channel = message["content"].split()[1]
            message["content"] = "A user has changed rooms..."


        return message

    def on_receive(self, message):
        if "channel" in message and message["channel"] != self.channel:
            return {"type": "empty"}

        return message



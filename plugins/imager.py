class Plugin():
    def on_send(self, message):
        return message

    def on_receive(self, message):
        if "content" in message and message["content"].startswith("https://"):
            if message["content"].endswith("png") or message["content"].endswith(".jpg"):
                message["content"] = f'<img src="{message["content"]}" height ="42" width ="42">'
            elif message["content"].endswith("mp4"):
                message["content"] = f'<video width="300" height="175" controls> <source src="{message["content"]}" type="video/mp4"> </video>'
            elif message["content"].endswith("ogg"):
                message["content"] = f'<video width="300" height="175" controls> <source src="{message["content"]}" type="video/ogg"> </video>'
            elif message["content"].find("youtube.com"):
                magicId = message["content"].split("=")[1]
                message["content"] = f'<iframe width="560" height="315" src="https://www.youtube.com/embed/{magicId}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'


        return message


"""
This plugin prints all messages it receives/sends to your terminal.
"""

class Plugin():
    def on_send(self, message):
        print("Sending", message)
        return message

    def on_receive(self, message):
        print("Receiving", message)
        return message

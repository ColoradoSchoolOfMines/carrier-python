"""
A server for sending messages between clients
"""
import asyncio
import json
import websockets

# Listen in on local and remote traffic coming in with this IP
HOST = "0.0.0.0"
PORT = 8765
print("Started the server...")


class Main:
    """
    The main websocket server and core of the program
    """

    def __init__(self):
        """
        Create the initial message list and user list
        """
        self.messages = [
            {"type": "text", "from": "server", "content": "Welcome to the server!"}
        ]
        # It's useful to prevent people from calling themselves nothing
        # or the server
        self.users = {"": None, "server": None}

    async def new_client(self, websocket, path):
        """
        Create a new client and listen to all their messages
        """
        # The name starts out empty
        name = ""

        # We need to get the user's name.
        # We assume that's the first thing the user sends.
        # The user's name needs to be unique.
        while name in self.users:
            await websocket.send("?")
            name = await websocket.recv()
        await websocket.send("ok")
        self.users[name] = websocket

        try:
            # Send the list of previous messages
            await websocket.send(json.dumps(self.messages))
            # Start the main loop
            while True:
                # Load their message for us
                raw_message = await websocket.recv()
                message = json.loads(raw_message)

                # Let's make sure they aren't impersonating people
                # by making sure the from field is correct
                message["from"] = name

                # Add the new message then drop any old ones
                # if there are more than 100.
                self.messages.append(message)
                self.messages = self.messages[:100]

                # Get all the users
                for user in self.users:
                    # Send it if they are real
                    if self.users[user]:
                        await self.users[user].send(json.dumps(message))
        except Exception:
            # Remove a client once they disconnect
            del self.users[name]


# Start the main loop
SERVER = websockets.serve(Main().new_client, HOST, PORT)

asyncio.get_event_loop().run_until_complete(SERVER)
asyncio.get_event_loop().run_forever()

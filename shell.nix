let
  pkgs = import <nixpkgs> {};
  python38 = let
    packageOverrides = self: super: {
      parso = super.parso.overridePythonAttrs(old: rec {
        version="0.7.0";
        src = super.fetchPypi {
          pname = old.pname;
          inherit version;
          sha256 = "908e9fae2144a076d72ae4e25539143d40b8e3eafbaeae03c1bfe226f4cdf12c";
        };
      });
      jedi = super.jedi.overridePythonAttrs(old: rec {
        version="0.17.0";
        src = super.fetchPypi {
          pname = old.pname;
          inherit version;
          sha256 = "df40c97641cb943661d2db4c33c2e1ff75d491189423249e989bcea4464f3030";
        };
      });
      asyncqt = super.pythonPackages.buildPythonPackage rec {
        name = "asyncqt";
        version = "0.8.0";
        src = super.fetchPypi {
          pname = name;
          inherit version;
          sha256 = "0gw8rb7f8jwqzakwkxx3kcadl0dys52fvk2ssgdlw7abg8y9kah7";
        };
        propagatedBuildInputs = [super.pythonPackages.pyqt5];
        checkPhase = ''
          echo "Don't run tests"
        '';
      };
    };
    in pkgs.python38.override {inherit packageOverrides;};
in
pkgs.mkShell {
  nativeBuildInputs = [ pkgs.qt5.wrapQtAppsHook ];
  buildInputs = [
    (pkgs.qt5.env "qt5-carrierPython-5" [pkgs.qt5.qtwebengine pkgs.qt5.qtbase])
    (python38.withPackages (ps: with ps; [ requests jedi pylint pynvim autopep8 pyqt5 websockets asyncqt pyqtwebengine]))
  ];
}
